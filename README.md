Snaap Exercise

## Install dependencies

Back-End
```bash
cd ./back-end/ && npm i && ../
```

Front-End
```bash
cd ./front-end/ && yarn install && ../
```

## Start project

Back-End
```bash
cd ./back-end/ && npm run start
```

Front-End
```bash
cd ./front-end/ && npm run start
```

## TODO

- Choose different library for products cards. Currently, the bootstrap card-columns order vertically. See (https://stackoverflow.com/questions/38704826/how-do-i-make-card-columns-order-horizontally/38705192).
- Make separate redux for product view
- Deploy application to Heroku
- Front-End & Back-end tests
