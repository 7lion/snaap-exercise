import React, {Fragment} from 'react';
import {CardColumns} from 'react-bootstrap';
import {useSelector} from 'react-redux';

import ListItem from '../../components/Products/ListItem';
import PromotionCard from '../../components/Products/PromotionCard';

const ListItems = () => {
  const {
    isFetching,
    hasError,
    errorMessage,
    products,
    promotion,
  } = useSelector(({products, appData}) => ({
    isFetching: products.isFetching,
    hasError: products.hasError,
    products: products.data,
    promotion: appData.promotion,
  }));

  if (hasError) {
    return <div className="d-flex justify-content-center">Error: {errorMessage}</div>;
  }
  if (isFetching) {
    return <div className="d-flex justify-content-center">Loading...</div>;
  }

  const showPromotion = promotion.hide === false;

  return (
   <CardColumns>
     {products.map((product, index) => (
      <Fragment key={product.id}>
        {showPromotion && promotion.order === index && (
          <PromotionCard promotion={promotion} />
        )}

        <ListItem product={product}/>
      </Fragment>
     ))}
   </CardColumns>
  );
};

export default ListItems;
