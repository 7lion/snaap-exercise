import React from 'react';
import {Card} from 'react-bootstrap';

const PromotionCard = ({promotion}) => {
  return (
   <Card className="promotion">{promotion.text}</Card>
  );
};

export default PromotionCard;
