import React, {useEffect} from 'react';
import {Form, Row, Col, Button} from 'react-bootstrap';
import {useDispatch, useSelector} from 'react-redux';

import useDebounce from '../../hooks/useDebounce';
import {changeProductsFilter, resetProductsFilter, loadProducts} from '../../redux/actions/products';

const ListFilter = () => {
  const dispatch = useDispatch();

  const {filter, vendors} = useSelector(({
    products: {filter}, appData: {vendors}
  }) => ({filter, vendors}));

  const {searchTerm, vendor} = filter;
  const debouncedSearchTerm = useDebounce(searchTerm, 1000);

  useEffect(() => {
    dispatch(loadProducts(filter));
  }, [debouncedSearchTerm, vendor, dispatch]);

  const onChangeFilter = (key, value) => {
    dispatch(changeProductsFilter(key, value));
  };

  const onResetFilter = () => {
    dispatch(resetProductsFilter());
  };

  return (
   <div className="list-filter">
     <Form>
       <Row>
         <Col md={3}>
           <Form.Control
            placeholder="Search"
            value={searchTerm}
            onChange={(e) => onChangeFilter('searchTerm', e.target.value)}
           />
         </Col>
         <Col md={3}>
           <Form.Control
            as="select"
            value={vendor}
            onChange={(e) => onChangeFilter('vendor', e.target.value)}
           >
             <option value="">All vendors</option>
             {vendors.map((vendor) => (
              <option value={vendor} key={vendor}>{vendor}</option>
             ))}
           </Form.Control>
         </Col>
         <Col md={3}>
           <Button variant="light" onClick={onResetFilter}>Reset</Button>
         </Col>
       </Row>
     </Form>
   </div>
  );
};

export default ListFilter;
