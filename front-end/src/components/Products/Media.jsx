import React from 'react';

const Media = ({item}) => {
  if (!item) {
    return (<div>Stub image</div>);
  }

  return (
   <>
     {item.type === 'video' ? (
      <video className="img-fluid" controls>
        <source src={item.url} type="video/mp4"/>
        Your browser does not support the video tag.
      </video>
     ) : (
      <img className="img-fluid" src={item.url} alt=""/>
     )}
   </>
  );
};

export default Media;
