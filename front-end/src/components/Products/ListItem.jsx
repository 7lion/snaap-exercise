import React from 'react';
import {Card} from 'react-bootstrap';
import {NavLink} from 'react-router-dom';

import Media from './Media';
import {getProductMediaItem} from '../../services/products';

const ListItem = ({product: {id, media, name, vendor}}) => {
  return (
   <Card>
     <Media item={getProductMediaItem(media)} />
     <Card.Body>
       <NavLink
        className="card-title h5"
        key={id}
        to={`/products/${id}`}
       >
         {name}
       </NavLink>
       <Card.Text>{vendor}</Card.Text>
     </Card.Body>
   </Card>
  );
};

export default ListItem;
