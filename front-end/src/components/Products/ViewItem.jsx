import React from 'react';
import {useSelector} from 'react-redux';
import {Card, CardColumns} from 'react-bootstrap';

import Media from './Media';

const ListItems = ({product}) => {
  const {
    isFetching,
    hasError,
    errorMessage,
  } = useSelector(({products}) => (products));

  if (hasError) {
    return <div className="d-flex justify-content-center">Error: {errorMessage}</div>;
  }
  if (isFetching) {
    return <div className="d-flex justify-content-center">Loading...</div>;
  }

  if (!product) {
    return <div className="d-flex justify-content-center">Not Found!</div>;
  }

  const {name, vendor, media} = product;

  return (
   <>
     <h1>{name}</h1>

     <p>
       Vendor: {vendor}
     </p>

     <p>Media:</p>

     <CardColumns>
       {media.map((item) => (
        <Card key={item.id}>
          <Media item={item} />
        </Card>
       ))}
     </CardColumns>
   </>
  );
};

export default ListItems;
