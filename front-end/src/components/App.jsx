import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {Container, Row, Col, Navbar} from 'react-bootstrap';

import Router from '../router';
import {loadAppData} from '../redux/actions/appData';

const App = () => {
  const dispatch = useDispatch();
  const {
    isFetching,
    hasError,
    errorMessage,
  } = useSelector(({appData}) => (appData));

  useEffect(() => dispatch(loadAppData()), [dispatch]);

  if (isFetching) {
    return <div className="d-flex justify-content-center">Loading...</div>;
  }

  if (hasError) {
    return <div className="d-flex justify-content-center">Error: {errorMessage}</div>;
  }

  return (
   <Container fluid>
     <Row>
       <Col>
         <Navbar bg="light">
           <Navbar.Brand href="#home">Snaap Exercise</Navbar.Brand>
         </Navbar>
         <Router/>
       </Col>
     </Row>
   </Container>
  );
};

export default App;
