import * as api from './api';
import {isFilterApplied} from './filter';

export const fetchProducts = async (filter) => {
  if (isFilterApplied(filter)) {
    return api.fetchProductsWithFilter(filter);
  }

  return api.fetchProducts().then((products) => {
    products.sort((a, b) => a.order - b.order);

    return products;
  });
};

export const getProductMediaItem = (media) => {
  let mediaItem = null;
  mediaItem = media.find((item) => item.type === 'video');

  if (mediaItem === null) {
    mediaItem = media.find((item) => item.type === 'image');
  }

  return mediaItem;
};
