import * as api from './api';

export const fetchAppData = async () => {
  return Promise.all([
    api.fetchVendors(),
    api.fetchPromotion(),
  ]).then(([vendors, promotion]) => ({vendors, promotion}));
};