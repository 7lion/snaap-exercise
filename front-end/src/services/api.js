/**
 * @TODO: Get variable from env file
 */
const API_URL = 'http://localhost:5000';

export const fetchProducts = async () => {
  return fetch(`${API_URL}/products`).then((response) => response.json());
};
export const fetchProductsWithFilter = async (filter) => {
  const urlParams = new URLSearchParams(Object.entries(filter));

  return fetch(`${API_URL}/products?${urlParams}`).then((response) => response.json());
};

export const fetchVendors = async () => {
  return fetch(`${API_URL}/vendors`).then((response) => response.json());
};

export const fetchPromotion = async () => {
  return fetch(`${API_URL}/promotion`).then((response) => response.json());
};
