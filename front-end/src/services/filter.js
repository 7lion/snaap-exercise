export const isFilterApplied = ({searchTerm, vendor}) => {
  return ![searchTerm, vendor].every((v) => !v);
};
