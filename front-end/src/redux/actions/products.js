import {fetchProducts} from '../../services/products';
import {
  LOAD_PRODUCTS_REQUEST,
  LOAD_PRODUCTS_SUCCESS,
  LOAD_PRODUCTS_FAILURE,
  CHANGE_PRODUCTS_FILTER,
  RESET_PRODUCTS_FILTER,
} from '../actionTypes';

const loadProductsRequest = () => ({
  type: LOAD_PRODUCTS_REQUEST,
  payload: {}
});

const loadProductsSuccess = (products) => ({
  type: LOAD_PRODUCTS_SUCCESS,
  payload: {products}
});

const loadPostsFailure = (error) => ({
  type: LOAD_PRODUCTS_FAILURE,
  payload: {error}
});

export const changeProductsFilter = (key, value) => ({
  type: CHANGE_PRODUCTS_FILTER,
  payload: {[key]: value}
});

export const resetProductsFilter = () => ({
  type: RESET_PRODUCTS_FILTER,
  payload: {}
});

export const loadProducts = (filter = {}) => (dispatch) => {
  dispatch(loadProductsRequest());

  fetchProducts(filter).then((products) => {
    dispatch(loadProductsSuccess(products));
  }, (err) => {
    dispatch(loadPostsFailure(err));
  });
};