import {fetchAppData} from '../../services/appData';
import {
  LOAD_APP_DATA_REQUEST, LOAD_APP_DATA_SUCCESS, LOAD_APP_DATA_FAILURE
} from '../actionTypes';

const loadAppDataRequest = () => ({
  type: LOAD_APP_DATA_REQUEST,
  payload: {}
});

const loadAppDataSuccess = (appData) => ({
  type: LOAD_APP_DATA_SUCCESS,
  payload: appData
});

const loadAppDataFailure = (error) => ({
  type: LOAD_APP_DATA_FAILURE,
  payload: {error}
});

export const loadAppData = () => (dispatch) => {
  dispatch(loadAppDataRequest());

  fetchAppData().then((appData) => {
    dispatch(loadAppDataSuccess(appData));
  }, (err) => {
    dispatch(loadAppDataFailure(err));
  });
};