import {combineReducers} from 'redux';

import ProductsReducer from './ProductsReducer';
import AppDataReducer from './AppDataReducer';

const reducers = combineReducers({
  products: ProductsReducer,
  appData: AppDataReducer,
});

export default reducers;
