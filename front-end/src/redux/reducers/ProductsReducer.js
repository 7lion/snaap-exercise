import {
  LOAD_PRODUCTS_REQUEST,
  LOAD_PRODUCTS_SUCCESS,
  LOAD_PRODUCTS_FAILURE,
  CHANGE_PRODUCTS_FILTER,
  RESET_PRODUCTS_FILTER,
} from '../actionTypes';

const initState = {
  isFetching: true,
  data: [],
  filter: {
    searchTerm: '',
    vendor: '',
  },
  errorMessage: '',
  hasError: false,
};

const ProductsReducer = (state = initState, {type, payload}) => {
  switch (type) {
    case LOAD_PRODUCTS_REQUEST:
      return {...state, isFetching: true};

    case LOAD_PRODUCTS_SUCCESS:
      return {...state, isFetching: false, data: payload.products, hasError: false, errorMessage: ''};

    case LOAD_PRODUCTS_FAILURE:
      return {...state, isFetching: false, hasError: true, errorMessage: payload.error.toString()};

    case CHANGE_PRODUCTS_FILTER:
      return {
        ...state,
        filter: {...state.filter, ...payload}
      };

    case RESET_PRODUCTS_FILTER:
      return {
        ...state,
        filter: {...initState.filter}
      };

    default:
      return state;
  }
};

export default ProductsReducer;
