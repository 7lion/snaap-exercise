import {
  LOAD_APP_DATA_REQUEST, LOAD_APP_DATA_SUCCESS, LOAD_APP_DATA_FAILURE
} from '../actionTypes';

const initState = {
  isFetching: true,
  vendors: [],
  promotion: null,
  errorMessage: '',
  hasError: false,
};

const AppDataReducer = (state = initState, {type, payload}) => {
  switch (type) {
    case LOAD_APP_DATA_REQUEST:
      return {...state, isFetching: true};

    case LOAD_APP_DATA_SUCCESS:
      return {...state, ...payload, isFetching: false};

    case LOAD_APP_DATA_FAILURE:
      return {...state, isFetching: false, hasError: true, errorMessage: payload.error.toString()};

    default:
      return state;
  }
};

export default AppDataReducer;
