import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import {BrowserRouter as Router} from 'react-router-dom';

import App from './components/App';
import store from './redux/store';

import 'bootstrap/dist/css/bootstrap.min.css';
import './assets/scss/base.scss';

ReactDOM.render(
 <Router>
   <Provider store={store}>
     <App/>
   </Provider>
 </Router>,
 document.getElementById('root')
);
