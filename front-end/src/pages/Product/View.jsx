import React, {useEffect} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import {Breadcrumb} from 'react-bootstrap';
import {useParams} from 'react-router-dom';

import {loadProducts} from "../../redux/actions/products";
import ViewItem from '../../components/Products/ViewItem';

const View = () => {
  const {id} = useParams();
  const dispatch = useDispatch();
  const products = useSelector(({products}) => (products.data));

  /**
   * @TODO: make separate redux for view the detailed product. API already exist.
   */
  useEffect(() => {
    if (products.length === 0) {
      dispatch(loadProducts());
    }
  }, [products, dispatch]);

  const product = products.find((product) => product.id === id);

  return (
   <>
     <Breadcrumb className="breadcrumbs">
       <Breadcrumb.Item>Home</Breadcrumb.Item>
       <Breadcrumb.Item href="/products">
         Products
       </Breadcrumb.Item>
       <Breadcrumb.Item active>
         {(product && product.name) || 'Loading ...'}
       </Breadcrumb.Item>
     </Breadcrumb>

     <ViewItem product={product} />
   </>
  );
};

export default View;
