import React, {useEffect} from 'react';
import {useDispatch} from 'react-redux';
import {Breadcrumb} from 'react-bootstrap';

import '../../assets/scss/list.scss';

import ListItems from '../../components/Products/ListItems';
import ListFilter from '../../components/Products/ListFilter';
import {loadProducts} from '../../redux/actions/products';

const List = () => {
  const dispatch = useDispatch();

  useEffect(() => dispatch(loadProducts()), [dispatch]);

  return (
   <>
     <Breadcrumb className="breadcrumbs">
       <Breadcrumb.Item>Home</Breadcrumb.Item>
       <Breadcrumb.Item active>
         Products
       </Breadcrumb.Item>
     </Breadcrumb>

     <ListFilter/>
     <ListItems/>
   </>
  );
};

export default List;
