import React from 'react';
import {Route, Redirect} from 'react-router-dom';

import ProductList from '../pages/Product/List';
import ProductView from '../pages/Product/View';

const Router = () => {
  return (
   <>
     <Route path="/products" component={ProductList} exact/>
     <Route path="/products/:id" component={ProductView}/>
     <Redirect exact from="/" to="/products"/>
   </>
  );
};

export default Router;
