const SEARCH_CACHE_TTL_IN_SECONDS = 300; // 5 min
const SEARCH_CACHE_KEY_PREFIX = 'products_search';

const sha1 = require('sha1');
const NodeCache = require('node-cache');
const cacheService = new NodeCache({stdTTL: SEARCH_CACHE_TTL_IN_SECONDS});

const {products} = require('./data');

const getHashFromObject = (obj) => {
  return `${SEARCH_CACHE_KEY_PREFIX}_${sha1(JSON.stringify(obj))}`;
};

const findByQuery = (searchParams) => {
  const {vendor} = searchParams;
  let {searchTerm} = searchParams;
  searchTerm = searchTerm.toLowerCase();

  const cacheKey = getHashFromObject(searchParams);

  const cacheProducts = cacheService.get(cacheKey);

  if (cacheProducts !== undefined) {
    return cacheProducts;
  }

  const searchProducts = products.filter((product) => {
    if (vendor && product.vendor !== vendor) {
      return false;
    }

    if (searchTerm && product.name.toLowerCase().indexOf(searchTerm) === -1) {
      return false;
    }

    return true;
  });

  cacheService.set(cacheKey, searchProducts);

  return searchProducts;
};

const list = () => products;
const findById = (id) => products.find((product) => product.id === id);
const findAll = () => products;

module.exports = {
  list,
  findById,
  findAll,
  findByQuery,
};
