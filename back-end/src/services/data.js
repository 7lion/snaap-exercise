const {vendors, promotion, products} = require('../data');

module.exports = {
  vendors,
  promotion,
  products,
};
