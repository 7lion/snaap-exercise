const express = require('express');
const cors = require('cors');
const dotenv = require('dotenv');
dotenv.config();

const app = express();
app.use(cors());

const {vendors, promotion} = require('./services/data');
const {findById, findByQuery, findAll} = require('./services/product');

app.get('/vendors', (req, res) => res.json(vendors));

app.get('/promotion', (req, res) => res.json(promotion));

app.get('/products', (req, res) => {
  const {searchTerm, vendor} = req.query;
  const isFilterApplied = ![searchTerm, vendor].every((v) => v === undefined);

  const products = isFilterApplied
   ? findByQuery({searchTerm, vendor})
   : findAll();

  res.json(products)
});

app.get('/products/:id', (req, res) => {
  const {id} = req.params;

  const product = findById(id);
  if (!product) {
    return res.sendStatus(404);
  }

  res.json(product)
});

app.listen(5000);